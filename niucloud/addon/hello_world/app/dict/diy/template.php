<?php

return [
    'DIY_HELLO_WORLD_INDEX' => [
        'title' => get_lang('dict_diy.page_hello_world_index'),
        'page' => '/hello_world/pages/index',
        'action' => ''
    ],
    'DIY_HELLO_WORLD_INFO' => [
        'title' => get_lang('dict_diy.page_hello_world_info'),
        'page' => '/hello_world/pages/info',
        'action' => ''
    ],
];